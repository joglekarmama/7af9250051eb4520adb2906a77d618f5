import React from 'react';
import ReactDOM from 'react-dom';
import WebFont from 'webfontloader';

WebFont.load({
  google: {
    families: ['Cinzel', 'serif'],
  },
});

import App from './app';
import './index.css';

ReactDOM.render(<App />, document.getElementById('root'));
